import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class ShiftArray {

    public static int[]shiftPositive(int[] arr){
        int temp;
        int[] a = Arrays.copyOf(arr, arr.length);
        int j = 0;
        for (int i = j; i < arr.length; i++) {
            if(a[i] > 0){
                System.out.println("i " + i);
                temp = a[i];
                for ( j = i+1; j < arr.length; j++) {
                    if(a[j] > 0){
                        temp = a[j];
                        a[i] = a[j];
                        System.out.println("j " + j);
                        j++;
                        break;
                    }
                }
            }
        }
        //System.out.println(Arrays.toString(arr));
        return a;
    }

    /*
    public static int[]shiftNegative(int[] arr){

    }*/

    public static int[] generateRandomArray(int size){
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            int r = (int)(Math.random()*200)-100;
            arr[i] = r;
        }
        return arr;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.printf("enter array size");
        int size = sc.nextInt();
        int[] arr = generateRandomArray(size);
        int[] shiftPositive = shiftPositive(arr);

        System.out.printf("array %s\n", Arrays.toString(arr));
        System.out.printf("shifted positive %s\n", Arrays.toString(shiftPositive));
    }
}
