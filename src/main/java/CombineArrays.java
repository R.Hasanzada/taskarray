import java.util.Arrays;
import java.util.Scanner;

public class CombineArrays {

    public static int[] generateArray(int n, boolean f){
        int[] arr = new int[n];
        if(f == true){
            for (int i = 0; i < n; i++) {
                int a = (int)(Math.random()*(100 + 1));
                if(a %2 == 0){
                    arr[i] = a;
                }else{
                    i--;
                }
            }
        }
        else{
            for (int i = 0; i < n; i++) {
                int a = (int)(Math.random()*(100 + 1));
                if(a %2 != 0){
                    arr[i] = a;
                }else{
                    i--;
                }
            }
        }
        return arr;
    }

    public static int[] combine(int[] odd, int[] even){
        int[]combineArray = new int[odd.length+even.length];
        for (int i = 0; i < combineArray.length; i+=2) {
            combineArray[i] = odd[i/2];
        }
        for (int i = 1; i < combineArray.length; i+=2) {
            combineArray[i] = even[i/2];
        }
        return combineArray;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.printf("enter the length of array\n");
        int N = sc.nextInt();
        int[] oddArrays = new int[N];
        int[] evenArray = new int[N];

        oddArrays = generateArray(N,false);
        evenArray = generateArray(N, true);


        System.out.printf("odd %s\n", Arrays.toString(oddArrays));
        System.out.printf("even %s\n", Arrays.toString(evenArray));
        System.out.printf("combine array %s\n", Arrays.toString(combine(oddArrays,evenArray)));

    }
}
